package net.neuralmetrics.eva.data;

import net.neuralmetrics.eva.calc.EvaSolution;
import net.neuralmetrics.eva.domain.aircraft.Aircraft;
import net.neuralmetrics.eva.domain.aircraft.AircraftSchedule;
import net.neuralmetrics.eva.domain.employee.Employee;
import net.neuralmetrics.eva.domain.employee.EmployeeAvailability;
import net.neuralmetrics.eva.domain.employee.EmployeeSkill;
import net.neuralmetrics.eva.domain.nightshift.NightShift;
import net.neuralmetrics.eva.domain.workorder.EmployeeRequirement;
import net.neuralmetrics.eva.domain.workorder.WorkOrder;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Load some sample data to the unsolved solution
 * Created by hoang on 16/07/2017.
 */
public class SampleDataLoader {
    private class NightShiftFiller
    {
        private NightShift newNightShift(Date d)
        {
            NightShift n = new NightShift();
            n.setDate(d);
            return n;
        }
        public void fillNightShiftForOneWeek(List<NightShift> nightShifts)
        {
            Calendar c = Calendar.getInstance();
            c.setTime(new Date());

            // Today
            NightShift n = newNightShift(c.getTime());
            nightShifts.add(n);

            for (int i=0; i<6; i++)
            {
                // Add 1 day to the day
                c.add(Calendar.DATE, 1);  // number of days to add
                n = newNightShift(c.getTime());
                nightShifts.add(n);
            }
        }
    }
    private class AircraftFiller
    {
        private Aircraft newAircraft(String name)
        {
            Aircraft a = new Aircraft();
            a.setName(name);
            return a;
        }
        private AircraftSchedule newAircraftSchedule(Aircraft aircraft, NightShift nightShift, int minuteOnGround)
        {
            AircraftSchedule as = new AircraftSchedule();
            as.setAircraft(aircraft);
            as.setNightShift(nightShift);
            as.setMinuteOnGround(minuteOnGround);
            return as;
        }
        private void fillAircraft(List<Aircraft> aircrafts, List<AircraftSchedule> aircraftSchedules, List<NightShift> nightShifts)
        {
            // VN-A600 aircraft will be available on 3 days
            Aircraft a = newAircraft("VN-A600");
            aircrafts.add(a);
            AircraftSchedule as = newAircraftSchedule(a, nightShifts.get(0), 120);
            aircraftSchedules.add(as);
            as = newAircraftSchedule(a, nightShifts.get(2), 300);
            aircraftSchedules.add(as);
            as = newAircraftSchedule(a, nightShifts.get(1), 120);
            aircraftSchedules.add(as);
        }
    }
    private class EmployeeFiller
    {
        private EmployeeSkill newSkill(EmployeeSkill.SKILL_LIST skill, Employee e)
        {
            EmployeeSkill s = new EmployeeSkill();
            s.setSkill(skill);
            s.setEmployee(e);
            return s;
        }
        private Employee newEmployee(String name)
        {
            Employee e = new Employee();
            e.setName(name);
            return e;
        }
        private EmployeeAvailability newEmployeeAvailablity(Employee employee, NightShift n)
        {
            EmployeeAvailability ea = new EmployeeAvailability();
            ea.setEmployee(employee);
            ea.setNightShift(n);
            return ea;
        }
        public void fillEmployee(List<Employee> employees, List<EmployeeSkill> employeeSkills, List<EmployeeAvailability> employeeAvailabilities, List<NightShift> nightShifts)
        {
            // First employee
            Employee e = newEmployee("Nguyen Van A1");
            employees.add(e);
            EmployeeSkill s = newSkill(EmployeeSkill.SKILL_LIST.A, e);
            employeeSkills.add(s);
            EmployeeAvailability ea = newEmployeeAvailablity(e, nightShifts.get(0));
            employeeAvailabilities.add(ea);

            // Second employee
            e = newEmployee("Nguyen Van A2");
            employees.add(e);
            s = newSkill(EmployeeSkill.SKILL_LIST.A, e);
            employeeSkills.add(s);
            ea = newEmployeeAvailablity(e, nightShifts.get(0));
            employeeAvailabilities.add(ea);

            // Third employee
            e = newEmployee("Nguyen Van B11");
            employees.add(e);
            s = newSkill(EmployeeSkill.SKILL_LIST.B1, e);
            employeeSkills.add(s);
            ea = newEmployeeAvailablity(e, nightShifts.get(0));
            employeeAvailabilities.add(ea);

            // Fourth employee
            e = newEmployee("Nguyen Van B12");
            employees.add(e);
            s = newSkill(EmployeeSkill.SKILL_LIST.B1, e);
            employeeSkills.add(s);
            ea = newEmployeeAvailablity(e, nightShifts.get(1));
            employeeAvailabilities.add(ea);

            // Fifth employee
            e = newEmployee("Nguyen Van B12");
            employees.add(e);
            s = newSkill(EmployeeSkill.SKILL_LIST.B1, e);
            employeeSkills.add(s);
            ea = newEmployeeAvailablity(e, nightShifts.get(1));
            employeeAvailabilities.add(ea);
        }
    }
    private class WorkOrderFiller
    {
        private WorkOrder newWorkOrder(Aircraft aircraft, int duration, Date deadline, String workDescription)
        {
            WorkOrder wo = new WorkOrder();
            wo.setAircraft(aircraft);
            wo.setDuration(duration);
            wo.setDeadline(deadline);
            wo.setWorkDescription(workDescription);
            return wo;
        }
        private EmployeeRequirement newEmployeeRequirement(EmployeeSkill.SKILL_LIST skill, int number, WorkOrder wo)
        {
            EmployeeRequirement er = new EmployeeRequirement();
            er.setSkill(skill);
            er.setNumber(number);
            er.setWorkOrder(wo);
            return er;
        }
        public void fillWorkOrder(List<Aircraft> aircrafts, List<WorkOrder> workOrders, List<EmployeeRequirement> employeeRequirements)
        {
            // Some date calculation
            Calendar c = Calendar.getInstance();
            c.setTime(new Date());

            // First work-order
            c.add(Calendar.DATE, -1); // Expire in 3 days
            WorkOrder wo = newWorkOrder(aircrafts.get(0), 30, c.getTime(), "Clean interior");
            workOrders.add(wo);
            EmployeeRequirement er = newEmployeeRequirement(EmployeeSkill.SKILL_LIST.A, 2, wo);
            employeeRequirements.add(er);

            // Second work-order
            c.add(Calendar.DATE, 1); // Expire in 3 days
            wo = newWorkOrder(aircrafts.get(0), 120, c.getTime(), "Clean exterior");
            workOrders.add(wo);
            er = newEmployeeRequirement(EmployeeSkill.SKILL_LIST.B1, 2, wo);
            employeeRequirements.add(er);

            // Third work-order
            c.add(Calendar.DATE, 0); // Expire in 3 days
            wo = newWorkOrder(aircrafts.get(0), 60, c.getTime(), "Clean something else");
            workOrders.add(wo);
            er = newEmployeeRequirement(EmployeeSkill.SKILL_LIST.A, 2, wo);
            employeeRequirements.add(er);
        }
    }

    private void initListInstances(EvaSolution sln)
    {
        sln.setAircrafts(new ArrayList<Aircraft>());
        sln.setAircraftSchedules(new ArrayList<AircraftSchedule>());
        sln.setEmployeeAvailabilities(new ArrayList<EmployeeAvailability>());
        sln.setEmployeeRequirements(new ArrayList<EmployeeRequirement>());
        sln.setEmployees(new ArrayList<Employee>());
        sln.setNightShifts(new ArrayList<NightShift>());
        sln.setWorkOrders(new ArrayList<WorkOrder>());
        sln.setEmployeeSkills(new ArrayList<EmployeeSkill>());
    }

    public void fillSolution(EvaSolution sln)
    {
        initListInstances(sln);
        NightShiftFiller nsf = new NightShiftFiller();
        nsf.fillNightShiftForOneWeek(sln.getNightShifts());
        AircraftFiller acf = new AircraftFiller();
        acf.fillAircraft(sln.getAircrafts(),sln.getAircraftSchedules(),sln.getNightShifts());
        EmployeeFiller ef = new EmployeeFiller();
        ef.fillEmployee(sln.getEmployees(),sln.getEmployeeSkills(),sln.getEmployeeAvailabilities(),sln.getNightShifts());
        WorkOrderFiller wof = new WorkOrderFiller();
        wof.fillWorkOrder(sln.getAircrafts(),sln.getWorkOrders(),sln.getEmployeeRequirements());
    }
}
