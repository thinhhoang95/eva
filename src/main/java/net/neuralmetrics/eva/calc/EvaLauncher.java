package net.neuralmetrics.eva.calc;

import net.neuralmetrics.eva.data.ResultPresenter;
import net.neuralmetrics.eva.data.SampleDataLoader;
import org.optaplanner.core.api.solver.Solver;
import org.optaplanner.core.api.solver.SolverFactory;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Overnight work pack optimisation algorithm for VJC
 * Operations Research at VJC
 * Created by hoang on 10/07/2017.
 */

public class EvaLauncher {
//    ***************************************
//    Properties
//    ***************************************

    private static SimpleDateFormat logTimeFormatter = new SimpleDateFormat("HH:mm:ss.SSS");

//    ***************************************
//    Some helper voids
//    ***************************************

    private static void println(String str)
    {
        System.out.println(str);
    }

    private static String getTimeStamp()
    {
        return logTimeFormatter.format(Calendar.getInstance().getTime());
    }

    public static void main(String[] args)
    {
        println("NeuralMetrics General Intelligence (NeuralMetrics IG)");
        println("Project EVA - Overnight work pack scheduling algorithm for VJC");
        println("Written by Hoang Dinh Thinh (thinh@neuralmetrics.net)");
        println("--------------------------------------------------------------");
        println("For academic purpose only. Do not use in production");
        println("--------------------------------------------------------------");

        // For result interpretation
        ResultPresenter resultPresenter = new ResultPresenter();
        // For logging timestamps



        println(getTimeStamp()+"   1. Loading data into solution...");
        // Load data into unsolved solution
        EvaSolution unSolvedSolution = new EvaSolution();
        SampleDataLoader dataLoader = new SampleDataLoader();
        dataLoader.fillSolution(unSolvedSolution);
        println("--------------------------------------------------------------");
        println("> WORK ORDERS");
        resultPresenter.setSolution(unSolvedSolution);
        resultPresenter.showWorkOrders();
        println("--------------------------------------------------------------");
        // Prepare the ProblemFactProvider
        ProblemFactProvider.setAircraftSchedules(unSolvedSolution.getAircraftSchedules());
        ProblemFactProvider.setEmployeeSkills(unSolvedSolution.getEmployeeSkills());

        println(getTimeStamp()+ "   2. Configuring solver...");
        // Configure solver
        SolverFactory<EvaSolution> solverFactory = SolverFactory.createFromXmlResource(
                "net/neuralmetrics/eva/evaSolverConfig.xml");
        Solver<EvaSolution> solver = solverFactory.buildSolver();

        println(getTimeStamp() + "   3. Solving...");
        // Solve the problem
        EvaSolution solvedSolution = solver.solve(unSolvedSolution);
        println("--------------------------------------------------------------");
        println(getTimeStamp()+"   Solving complete");
        println("--------------------------------------------------------------");

        // Display result
        resultPresenter.setSolution(solvedSolution);
        resultPresenter.showScore();
        resultPresenter.showWorkOrderShift();
    }
}
