package net.neuralmetrics.eva.calc;

import net.neuralmetrics.eva.domain.aircraft.Aircraft;
import net.neuralmetrics.eva.domain.aircraft.AircraftSchedule;
import net.neuralmetrics.eva.domain.employee.Employee;
import net.neuralmetrics.eva.domain.employee.EmployeeAvailability;
import net.neuralmetrics.eva.domain.employee.EmployeeSkill;
import net.neuralmetrics.eva.domain.nightshift.NightShift;
import net.neuralmetrics.eva.domain.workorder.EmployeeRequirement;
import net.neuralmetrics.eva.domain.workorder.WorkOrder;
import org.optaplanner.core.api.domain.solution.PlanningEntityCollectionProperty;
import org.optaplanner.core.api.domain.solution.PlanningScore;
import org.optaplanner.core.api.domain.solution.PlanningSolution;
import org.optaplanner.core.api.domain.solution.drools.ProblemFactCollectionProperty;
import org.optaplanner.core.api.score.buildin.hardmediumsoft.HardMediumSoftScore;

import java.util.List;

/**
 * This is a scheme for a solution that EVA generates
 * Created by hoang on 15/07/2017.
 */

@PlanningSolution
public class EvaSolution {
    private List<Aircraft> aircrafts;
    private List<AircraftSchedule> aircraftSchedules;
    private List<Employee> employees;
    private List<EmployeeAvailability> employeeAvailabilities;
    private List<EmployeeSkill> employeeSkills;
    private List<NightShift> nightShifts;
    private List<WorkOrder> workOrders;
    private List<EmployeeRequirement> employeeRequirements;
    private HardMediumSoftScore score;

    @ProblemFactCollectionProperty
    public List<Aircraft> getAircrafts() {
        return aircrafts;
    }

    public void setAircrafts(List<Aircraft> aircrafts) {
        this.aircrafts = aircrafts;
    }

    @ProblemFactCollectionProperty
    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    @ProblemFactCollectionProperty
    public List<NightShift> getNightShifts() {
        return nightShifts;
    }

    public void setNightShifts(List<NightShift> nightShifts) {
        this.nightShifts = nightShifts;
    }

    @PlanningEntityCollectionProperty
    public List<WorkOrder> getWorkOrders() {
        return workOrders;
    }

    public void setWorkOrders(List<WorkOrder> workOrders) {
        this.workOrders = workOrders;
    }

    @PlanningScore
    public HardMediumSoftScore getScore() {
        return score;
    }

    public void setScore(HardMediumSoftScore score) {
        this.score = score;
    }

    @ProblemFactCollectionProperty
    public List<AircraftSchedule> getAircraftSchedules() {
        return aircraftSchedules;
    }

    public void setAircraftSchedules(List<AircraftSchedule> aircraftSchedules) {
        this.aircraftSchedules = aircraftSchedules;
    }

    @ProblemFactCollectionProperty
    public List<EmployeeAvailability> getEmployeeAvailabilities() {
        return employeeAvailabilities;
    }

    public void setEmployeeAvailabilities(List<EmployeeAvailability> employeeAvailabilities) {
        this.employeeAvailabilities = employeeAvailabilities;
    }

    @ProblemFactCollectionProperty
    public List<EmployeeRequirement> getEmployeeRequirements() {
        return employeeRequirements;
    }

    public void setEmployeeRequirements(List<EmployeeRequirement> employeeRequirements) {
        this.employeeRequirements = employeeRequirements;
    }

    @ProblemFactCollectionProperty
    public List<EmployeeSkill> getEmployeeSkills() {
        return employeeSkills;
    }

    public void setEmployeeSkills(List<EmployeeSkill> employeeSkills) {
        this.employeeSkills = employeeSkills;
    }
}
