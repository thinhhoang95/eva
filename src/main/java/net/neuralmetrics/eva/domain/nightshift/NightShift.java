package net.neuralmetrics.eva.domain.nightshift;

import org.optaplanner.core.api.domain.entity.PlanningEntity;

import java.util.Date;

/**
 * A night shift is a work pack
 * Created by hoang on 10/07/2017.
 */

public class NightShift {
    private Date date;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
