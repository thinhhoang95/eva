package net.neuralmetrics.eva.domain.employee;

/**
 * Created by hoang on 15/07/2017.
 */
public class EmployeeSkill {

    public enum SKILL_LIST {A, B1, B2, ENG, STRUCT}
    private SKILL_LIST skill;
    private Employee employee;

    public SKILL_LIST getSkill() {
        return skill;
    }

    public void setSkill(SKILL_LIST skill) {
        this.skill = skill;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
}
