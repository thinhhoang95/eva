package net.neuralmetrics.eva.domain.employee;

import net.neuralmetrics.eva.calc.ProblemFactProvider;
import net.neuralmetrics.eva.domain.nightshift.NightShift;

import java.util.List;

/**
 * Created by hoang on 16/07/2017.
 */
public class EmployeeAvailability {
    private Employee employee;
    private NightShift nightShift;

    public Employee getEmployee() {
        return employee;
    }

    // Find and return the employee skill record of this employee
    // Must initialise ProblemFactProvider before use
    public boolean hasSkill(EmployeeSkill.SKILL_LIST skill) {
        List<EmployeeSkill> employeeSkills = ProblemFactProvider.employeeSkills;
        for (EmployeeSkill es : employeeSkills)
        {
            if (es.getEmployee()==employee)
            {
                if(es.getSkill()==skill) return true;
            }
        }
        return false;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public NightShift getNightShift() {
        return nightShift;
    }

    public void setNightShift(NightShift nightShift) {
        this.nightShift = nightShift;
    }
}
