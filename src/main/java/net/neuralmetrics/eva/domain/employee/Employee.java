package net.neuralmetrics.eva.domain.employee;

import java.util.List;

/**
 * Created by hoang on 15/07/2017.
 */
public class Employee {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
