package net.neuralmetrics.eva.domain.workorder;

import net.neuralmetrics.eva.calc.ProblemFactProvider;
import net.neuralmetrics.eva.domain.aircraft.Aircraft;
import net.neuralmetrics.eva.domain.aircraft.AircraftSchedule;
import net.neuralmetrics.eva.domain.nightshift.NightShift;
import org.optaplanner.core.api.domain.entity.PlanningEntity;
import org.optaplanner.core.api.domain.valuerange.ValueRangeProvider;
import org.optaplanner.core.api.domain.variable.PlanningVariable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by hoang on 10/07/2017.
 */

@PlanningEntity
public class WorkOrder {
    private String workDescription;
    private int duration; // in minutes
    private Date deadline; // must be done before
    private Aircraft aircraft;
    private NightShift nightShift;

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public Aircraft getAircraft() {
        return aircraft;
    }

    public void setAircraft(Aircraft aircraft) {
        this.aircraft = aircraft;
    }

    @PlanningVariable(valueRangeProviderRefs = "nightShiftRange", nullable = true)
    public NightShift getNightShift() {
        return nightShift;
    }

    public void setNightShift(NightShift nightShift) {
        this.nightShift = nightShift;
    }

    public String getWorkDescription() {
        return workDescription;
    }

    public void setWorkDescription(String workDescription) {
        this.workDescription = workDescription;
    }

    // valueRangeProviders
    // The list of night shifts for nightShiftRange to choose from must have the aircraft available
    // This method use ProblemFactProvider thus it must be initialised before use
    @ValueRangeProvider(id = "nightShiftRange")
    public List<NightShift> nightShiftsAircraftIsHere()
    {
        List<NightShift> filteredResult = new ArrayList<NightShift>();
        for (AircraftSchedule as : ProblemFactProvider.aircraftSchedules)
        {
            if(as.getAircraft().equals(aircraft))
            {
                filteredResult.add(as.getNightShift());
            }
        }
        return filteredResult;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public WorkOrder getWorkOrder()
    {
        return this;
    }

    public boolean equals(WorkOrder target)
    {
        if (target.getDeadline().equals(deadline) && target.getAircraft().equals(aircraft)) return true;
        else return false;
    }

    // Some helper methods
    @Override
    public String toString()
    {
        return "WO AC: "+aircraft.getName()+" DL: "+deadline.toString();
    }
}
