package net.neuralmetrics.eva.domain.aircraft;

import net.neuralmetrics.eva.domain.nightshift.NightShift;

/**
 * Created by hoang on 11/07/2017.
 */
public class AircraftSchedule {
    private Aircraft aircraft;
    private NightShift nightShift;
    private int minuteOnGround;

    public NightShift getNightShift() {
        return nightShift;
    }

    public void setNightShift(NightShift nightShift) {
        this.nightShift = nightShift;
    }

    public int getMinuteOnGround() {
        return minuteOnGround;
    }

    public void setMinuteOnGround(int minuteOnGround) {
        this.minuteOnGround = minuteOnGround;
    }

    public Aircraft getAircraft() {
        return aircraft;
    }

    public void setAircraft(Aircraft aircraft) {
        this.aircraft = aircraft;
    }
}
